#!/bin/sh
racket new-server.rkt > drive-output.$(date '+%Y%m%d%H%M%S').txt 2>&1 &
serverpid=$!
echo "Serverpid is $serverpid"
while true
do
    if netstat -an | grep -q '2322.*LISTEN'
    then
	break
    fi
    sleep 0.1
done
echo '(+ 1 2 3 4 5 6)' | ssh localhost -p 2322
sleep 1
kill -INT $serverpid
echo "Killed $serverpid"
