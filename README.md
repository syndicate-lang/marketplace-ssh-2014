# Racket SSH server (and client)

This is a [Racket](http://racket-lang.org/) implementation of the SSH
v2 protocol. It's written to work with
[Marketplace](https://github.com/tonyg/marketplace), but could readily
be adapted to work with other I/O substrates. (It originally used
Racket's `sync` and events directly.)

The code is not quite fully separated into a reusable library yet: at
present, it is a hard-coded Racket REPL server. Changing this is
straightforward, but low-priority right now. Patches welcome!

## How to compile and run the code

You will need the latest **prerelease** version of Racket. Any version
newer than or equal to Racket 5.3.4.10 should work. Nightly-build
installers for Racket can be downloaded
[here](http://pre.racket-lang.org/installers/).

Once you have Racket installed,

    raco pkg install marketplace bitsyntax

to install Marketplace (note: will take a long time) and
[bitsyntax](https://github.com/tonyg/racket-bitsyntax/), and then

    make

(or the equivalent, `raco make new-server.rkt`) to compile the SSH
code. Once it has compiled successfully,

    racket new-server.rkt

will start the server running on `localhost` port 2322. Log in to the
server with

    ssh localhost -p 2322

To enable debug output, try

    MATRIX_LOG=info racket new-server.rkt

## Copyright and License

Copyright 2010, 2011, 2012, 2013 Tony Garnock-Jones <tonyg@ccs.neu.edu>

This file is part of marketplace-ssh.

marketplace-ssh is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

marketplace-ssh is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with marketplace-ssh. If not, see
<http://www.gnu.org/licenses/>.
